const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/assets/styles/fonts.scss";
          @import "@/assets/styles/main.scss";
        `,
      },
    },
  },
});
