import { ref } from "vue";

export type Post = {
  id: number;
  title: string;
  body: string;
};

export default function usePosts() {
  const post = ref<Post | null>(null);
  const posts = ref<Post[] | null>(null);

  async function fetchPosts() {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts`);
    posts.value = await response.json();
  }

  async function fetchPost(postId) {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${postId}`
    );
    post.value = await response.json();
  }

  return {
    fetchPost,
    fetchPosts,
    post,
    posts,
  };
}
