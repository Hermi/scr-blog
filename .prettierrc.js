module.exports = {
  semi: true,
  bracketSpacing: true,
  arrowParens: "avoid",
  tabWidth: 2,
};
